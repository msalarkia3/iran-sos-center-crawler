from crawler import SOSCrawler
from dbs import CSVDatabase

if __name__ == '__main__':
    database = CSVDatabase('centers.csv')
    sos_crawler = SOSCrawler(database)
    sos_crawler.crawl()
    sos_crawler.save()
