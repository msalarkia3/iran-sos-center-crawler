from bs4 import BeautifulSoup
import requests

GENERAL_HEADERS = {
    'accept': '*/*',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'en-US,en;q=0.9,fa-IR;q=0.8,fa;q=0.7',
    'cache-control': 'no-cache',
    'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'origin': 'https://www.iranassistance.com',
    'pragma': 'no-cache',
    'referer': 'https://www.iranassistance.com/carecenter/index',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-origin',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
                  'Chrome/87.0.4280.141 Safari/537.36',
    'x-requested-with': 'XMLHttpRequest',
}


class PageRow:
    COLUMNS = ['عنوان', 'نوع قرارداد', 'استان', 'شهر', 'صدور معرفی‌نامه', 'معرفی‌نامه آنلاین', '_id']

    def __init__(self, row):
        self.cols = row.select('.sos-col')

    @property
    def data(self):
        data = {}
        for i, col in enumerate(self.cols):
            try:
                if i < 4:
                    output = col.text.strip()
                elif i < 6:
                    output = col.select_one('i').get('title')
                else:
                    output = col.select_one('.fancybx').get('href').split('/')[-1]
            except:
                output = None

            data[PageRow.COLUMNS[i]] = output

        data.update(Popup(data['_id']).additional_data)

        return data


class Popup:
    def __init__(self, center_id):
        self.id = center_id

    @property
    def additional_data(self):
        response = requests.get(f'https://www.iranassistance.com/CareCenter/Popup/{self.id}')
        soup = BeautifulSoup(response.text, 'html.parser')
        titles = [x.text.strip()[:-1] for x in soup.select('dt')]
        values = [x.text.strip() for x in soup.select('dd')]
        data = {}
        for title, value in zip(titles, values):
            data[title] = value

        return data
