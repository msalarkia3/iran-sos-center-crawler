import requests
from bs4 import BeautifulSoup
from .auxilary import PageRow, GENERAL_HEADERS


class SOSCrawler:
    def __init__(self, db):
        self.base_url = 'https://www.iranassistance.com/carecenter/index'
        self.session = requests.Session()
        self._main_page_response = None
        self._cookie = None
        self._token = None
        self.db = db
        self.all_data = []

    @property
    def main_page_response(self):
        if self._main_page_response is not None:
            return self._main_page_response

        self._main_page_response = self.session.get(
            'https://www.iranassistance.com/carecenter/index', headers={
                'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
                              'Chrome/87.0.4280.141 Safari/537.36'
            })
        return self._main_page_response

    @property
    def cookie(self):
        if self._cookie is not None:
            return self._cookie

        try:
            for key in self.main_page_response.headers:
                if key.lower() == 'set-cookie':
                    self._cookie = '__RequestVerificationToken=' + \
                                   self.main_page_response.headers[key].split('=')[1].split(';')[0]
        except (IndexError, KeyError) as e:
            self._cookie = None

        return self._cookie

    @property
    def token(self):
        if self._token is not None:
            return self._token

        soup = BeautifulSoup(self.main_page_response.text, 'html.parser')
        return soup.select_one('input[name="__RequestVerificationToken"]').get('value')

    def save(self):
        self.db.save(self.all_data)

    def crawl_page(self, page_response):
        soup = BeautifulSoup(page_response.text, 'html.parser')
        rows = soup.select('.sos-table-row')
        centers_data = []
        for row in rows:
            centers_data.append(PageRow(row).data)

        return centers_data

    def crawl(self):
        for page in range(1, 251):
            page_response = self.session.post('https://www.iranassistance.com/CareCenter/Search', headers={
                'cookie': self.cookie,
                **GENERAL_HEADERS
            }, data={
                'page': f'{page}',
                'SearchString': '',
                'ProvinceId': '',
                'KindService': '',
                'Status': True,
                '__RequestVerificationToken': self.token,
            })
            self.all_data.extend(self.crawl_page(page_response))
            print(f'page {page} finished successfully!')
