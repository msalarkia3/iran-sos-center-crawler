import pandas as pd
from abc import abstractmethod, ABCMeta
import json


class AbstractDB(metaclass=ABCMeta):
    def __init__(self, file_name):
        self.file_name = file_name

    @abstractmethod
    def save(self, data):
        pass


class CSVDatabase(AbstractDB):
    def save(self, data):
        pd.DataFrame(data).drop(['_id'], axis=1).to_csv(self.file_name, index=False)
        return True


class JSONDatabase(AbstractDB):
    def save(self, data):
        with open(self.file_name, mode='w', encoding='utf-8') as f:
            json.dump(data, f, ensure_ascii=False)

        return True
